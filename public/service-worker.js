// self.addEventListener('fetch', (event) => {
//   const mainUrl = `/cellar/api/v1/download`;
//   console.log(event.request.url.includes(mainUrl));
//   if (event.request.url.includes(mainUrl)) {
// event.respondWith(
//   caches.match(event.request).then((cachedResponse) => {
//     if (cachedResponse) {
//       return cachedResponse;
//     }
//     return fetch(event.request).then((response) => {
//       return caches.open('navatel').then((cache) => {
//         cache.put(event.request, response.clone());
//         // limitInCache('navatel', 2).then(() => response);
//         return response;
//       });
//     });
//   }),
// );
//   }
// });

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  } else if (event.data && event.data.type === 'setData') {
  }
});

self.addEventListener('activate', async () => {
  try {
    const applicationServerKey =
      'BBO19b0QiG8Jvo8spacGG_OE2an-TaBrs27hMrEskR9mq4JQ6KMN__Izwe6Coy5LjNGZXK1-2JvNZBtkjKPkxNg';
    const options = { applicationServerKey, userVisibleOnly: true };
    let subscription = await self.registration.pushManager.getSubscription();
    if (subscription == null) {
      subscription = await self.registration.pushManager.subscribe(options);
    }
  } catch (err) {}
});

// //Web Push Notifications//
// let body
self.addEventListener('push', function (event) {
  const payload = JSON.parse(event.data.text());
  if (
    payload.data?.body == 'voipcall' ||
    payload.data?.service_type == 'call'
  ) {
    const options = {
      body: payload.data && payload.data.nickname ? payload.data.nickname : ' ',
      silent: false,
      title: 'Incoming Call',
      // icon: "push_message.notification.icon",
      // image: push_message.notification.image,
      // vibrate: [200, 100, 200, 100, 200, 100, 200],
      tag: 'alert',
      actions: [
        {
          action: '/callpad',
          title: 'Show',
        },
      ],
    };
    event.waitUntil(self.registration.showNotification(`Call`, options));
  }
  if (
    payload.data?.service_type == 'oto' ||
    payload.data?.service_type == 'grp'
  ) {
    const options = {
      body: payload.data && payload.data.nickname ? payload.data.nickname : ' ',
      silent: false,
      title: 'Message',
      // icon: "push_message.notification.icon",
      // image: push_message.notification.image,
      // vibrate: [200, 100, 200, 100, 200, 100, 200],
      tag: 'alert',
      actions: [
        {
          action: '/message',
          title: 'Show',
        },
      ],
    };
    event.waitUntil(self.registration.showNotification(`Message`, options));
  }
});

self.addEventListener('notificationclick', function (event) {
  const baseUrl = window?.baseUrl || 'https://app.navatel.ir/';
  const clickedNotification = event.notification;
  clickedNotification.close();
  const promiseChain = self.clients.openWindow(baseUrl + 'callpad');
  event.waitUntil(promiseChain);
});
