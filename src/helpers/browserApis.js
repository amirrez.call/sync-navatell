import { useOverallChatsStore } from '@/store/chats/overall.js';
let wakeLock;
export function setScreenWakeLock(setOn = true) {
  if ('WakeLock' in window && 'request' in window.WakeLock) {
    const requestWakeLock = () => {
      const controller = new AbortController();
      const signal = controller.signal;
      window.WakeLock.request('screen', { signal }).catch((e) => {});
      return controller;
    };
    if (setOn) {
      if (wakeLock) {
        wakeLock.abort();
        wakeLock = null;
        wakeLock = requestWakeLock();
        return;
      }
      wakeLock = requestWakeLock();
    }
    if (!setOn && wakeLock) {
      wakeLock.abort();
      wakeLock = null;
    }
  } else if ('wakeLock' in navigator && 'request' in navigator.wakeLock) {
    const requestWakeLock = async () => {
      try {
        wakeLock = await navigator.wakeLock.request('screen');
      } catch (e) {}
    };
    if (setOn) {
      if (wakeLock) {
        wakeLock.release();
        wakeLock = null;
        wakeLock = requestWakeLock();
        return;
      }
      wakeLock = requestWakeLock();
    }
    if (!setOn && wakeLock) {
      wakeLock.release();
      wakeLock = null;
    }
  }
}

export function handelBrowserTabVisibilityChange() {
  if (document.hidden !== undefined) {
    // Browser supports Page Visibility API

    // Attach visibility change event listener
    document.addEventListener('visibilitychange', handleVisibilityChange);

    // Function to handle visibility change
    function handleVisibilityChange() {
      if (document.hidden) {
        // User switched away from the tab (lost focus)
        // Add your code to handle this scenario
        console.log('tab is hidden');
      } else {
        console.log('tab now is on focus');

        setTimeout(() => {
          const overallChatStore = useOverallChatsStore();
          overallChatStore.getChatList();
        }, 0);

        // User switched back to the tab (regained focus)
        // Add your code to handle this scenario
      }
    }
  } else {
    // Browser does not support Page Visibility API
    console.log('Page Visibility API not supported');
  }
}

export function copyToClipboard(value) {
  navigator.clipboard
    .writeText(value)
    .then(() => {
      console.log('Text successfully copied to clipboard');
    })
    .catch((err) => {
      console.error('Error copying text to clipboard:', err);
    });
}
