import { useWebWorkerFn } from '@vueuse/core';
import {
  detectTypeOfMessage,
  detectReplyMessageType,
  calculateImageAndVideoAcceptRatio,
} from '@/helpers/chatMessageParser.js';
import {
  detectTextDirection,
  detetctTextIncludesLink,
} from '@/helpers/textFormatter.js';
import { bytesToMegabytes } from '@/helpers/formatter.js';
import { localDateToUTCConverter } from '@/helpers/dateAndTimeFormatter.js';

export const chatsParser = async (serverListData, userId, source) => {
  const { workerFn } = useWebWorkerFn(
    async (chatList, userId, screenDimension, source) => {
      const typeOfMessage = (information) => {
        if (['oto.txt', 'oto.jnd', 'oto.edt'].includes(information.mtype))
          return 'text';
        if (
          ['oto.img', 'oto.vid', 'oto.aud', 'oto.doc'].includes(
            information.mtype,
          )
        ) {
          return detectFileTypeForDownloading(information.data.type);
        }
        if (information.mtype == 'oto.mis') {
          return 'missCall';
        }
        return null;
      };

      const videoAsFileTypes = [
        'video/webm',
        'video/x-flv',
        'video/mkv',
        'video/matroska',
        'video/x-matroska',
        'video/divxplus',
      ];

      const videoAsAudioTypes = [
        'video/mpeg',
        'video/x-mpeg',
        'video/mkv',
        'video/matroska',
        'video/x-matroska',
        'video/divxplus',
      ];
      const replyMessageTypes = {
        text: 3,
        sticker: 4,
        image: 5,
        video: 6,
        audio: 7,
        file: 8,
        location: 9,
        notify: 10,
        deleted: 11,
        history: 12,
        WaitForResponse: 13,
        UnknownMessage: 14,
      };
      const detectReplyMessageType = (data) => {
        if (typeof data === 'number' || !isNaN(Number(data))) {
          const inputNumber = Number(data);
          const messageType = Object.keys(replyMessageTypes).find(
            (key) => replyMessageTypes[key] === inputNumber,
          );
          return messageType ? messageType : 'UnknownMessage';
        } else if (typeof data === 'string') {
          const messageType = replyMessageTypes[data];
          return messageType ? messageType : 'UnknownMessage';
        } else {
          return 'Unknown type';
        }
      };
      const detectFileTypeForDownloading = (type) => {
        const splitType = type.split('/')[0];
        if (splitType == 'image' && !type.includes('svg')) return 'image';
        if (
          splitType == 'video' &&
          !videoAsFileTypes.includes(type) &&
          !videoAsAudioTypes.includes(type)
        ) {
          return 'video';
        }
        if (splitType == 'audio' || videoAsAudioTypes.includes(type))
          return 'audio';

        return 'file';
      };

      function detectTextDirection(text) {
        if (!text) return null;
        text = text.replaceAll(' ', ''); // Remove spaces from the input text
        let isRightToLeft = null;
        for (let index = 0; index < text.length; index++) {
          const char = text[index];
          if (/^[\u0600-\u06FF\s]+$/.test(char)) {
            isRightToLeft = true;
            break; // Exit loop when a character is detected
          } else if (/^[A-Za-z0-9]*$/.test(char)) {
            isRightToLeft = false;
            break; // Exit loop when a character is detected
          }
        }
        return isRightToLeft;
      }
      function calculateImageAndVideoAcceptRatio(imageDimension) {
        const [imageWidth, imageHeight] = imageDimension.split('x');
        const originalWidth = imageWidth;
        const originalHeight = imageHeight;
        const maxWidth = (screenDimension.width * 80) / 100; // Set the maximum width for rendering
        const maxHeight = (screenDimension.height * 40) / 100; // Set the maximum height for rendering
        const aspectRatio = originalWidth / originalHeight;
        let renderedWidth, renderedHeight;

        if (originalWidth > maxWidth || originalHeight > maxHeight) {
          if (maxWidth / aspectRatio < maxHeight) {
            renderedWidth = maxWidth;
            renderedHeight = renderedWidth / aspectRatio;
          } else {
            renderedHeight = maxHeight;
            renderedWidth = renderedHeight * aspectRatio;
          }
        } else {
          // If the image is smaller than the maximum dimensions, keep its original size
          renderedWidth = originalWidth;
          renderedHeight = originalHeight;
        }
        return {
          renderedWidth,
          renderedHeight,
        };
      }
      function detetctTextIncludesLink(text) {
        const linkRegex = /(http(s)?:\/\/[^\s]+)/gi;
        const links = text.match(linkRegex);
        if (!links) return text;
        return renderTextWithAvailableLink(text);
      }
      function renderTextWithAvailableLink(text) {
        // Regular expression to find links in the string
        const linkRegex = /(https?:\/\/[^\s]+)/g;
        const stringWithHTMLLinks = text.replace(
          linkRegex,
          '<a href="$&" target="_blank">$&</a>',
        );
        return stringWithHTMLLinks;
      }
      function bytesToMegabytes(bytes) {
        return (bytes / (1024 * 1024)).toFixed(2); // Convert bytes to megabytes and round to 2 decimal places
      }
      const filteredList = [];
      for (const item of chatList) {
        const chat = {
          ...item,
          body: JSON.parse(item.body),
        };
        const itsMe = chat.from == userId;
        const type = typeOfMessage(chat.body);
        let content =
          type == 'text' || type == 'missCall'
            ? detetctTextIncludesLink(chat.message)
            : chat.body.data.fileId;
        const isReplied = Boolean(chat.body.data.ext_data?.replyMsgId);
        let repliedData = null;

        if (isReplied) {
          const text = chat.body.data.ext_data.replyDescription;
          const textDirectionIsRtl = detectTextDirection(text);
          repliedData = {
            messageText: text,
            targetMessageId: chat.body.data.ext_data.replyMsgId,
            textDirectionIsRtl,
            replyType: chat.body.data.ext_data.replyType,
            replyFileId: chat.body.data.ext_data.replyFileId,
            renderReplyType: detectReplyMessageType(
              chat.body.data.ext_data.replyType,
            ),
          };
        }
        const isEdited = Boolean(chat.body.edit_state);
        const editedInfo = !isEdited
          ? false
          : { isEdited, date: chat.modification_time };
        const dimension = chat.body.data?.dim
          ? calculateImageAndVideoAcceptRatio(chat.body.data.dim)
          : null;
        const information = {
          id: chat.guid,
          type,
          status: Boolean(chat.deliverstatus) ? 'seen' : 'sent',
          content,
          date: chat.creation_time,
          repliedData,
          editedInfo,
          textDirectionIsRtl:
            type == 'text' ? detectTextDirection(chat.message) : null,
          dimension,
          itsMe,
          source,
          additionalFileInfo: {
            ...chat.body.data,
            mbSize: bytesToMegabytes(chat.body?.data?.size),
            renderedType: chat.body?.data?.name
              ?.split('.')[1]
              .toLocaleUpperCase(),
          },
        };
        filteredList.push(information);
      }

      return filteredList.reverse();
    },
  );

  const result = await workerFn(
    serverListData,
    userId,
    {
      width: innerWidth,
      height: innerHeight,
    },
    source,
  );
  return result;
};

export function generateDataModelInChat(
  messageItem,
  itsMe,
  textDirectionIsRtl,
  type,
) {
  type = type ? type : detectTypeOfMessage(messageItem);
  let dimension = null;
  let content;
  let previewState;
  if (type != 'text') {
    const endsWithType = type.split('.')[1];
    dimension =
      ['vid', 'img'].includes(endsWithType) || ['video', 'image'].includes(type)
        ? calculateImageAndVideoAcceptRatio(messageItem.data.dim)
        : false;
    previewState = messageItem.previewBlob;
    content = messageItem.data.fileId;
  } else {
    content = detetctTextIncludesLink(messageItem.message);
    textDirectionIsRtl =
      textDirectionIsRtl != null
        ? textDirectionIsRtl
        : detectTextDirection(messageItem.message);
  }
  const isReplied = Boolean(messageItem.data.ext_data?.replyMsgId);
  let repliedData = null;
  if (isReplied) {
    const text = messageItem.data.ext_data.replyDescription;
    const textDirectionIsRtl = detectTextDirection(text);
    repliedData = {
      messageText: text,
      targetMessageId: messageItem.data.ext_data.replyMsgId,
      textDirectionIsRtl,
      renderReplyType: detectReplyMessageType(
        messageItem.data.ext_data.replyType,
      ),
      replyType: messageItem.data.ext_data.replyType,
      replyFileId: messageItem.data.ext_data.replyFileId,
    };
  }
  const information = {
    content,
    date: localDateToUTCConverter(new Date()).toString(),
    dimension,
    repliedData,
    editedInfo: false,
    status: 'sending',
    id: messageItem.id,
    itsMe: itsMe,
    textDirectionIsRtl: textDirectionIsRtl,
    type: detectTypeOfMessage(messageItem),
    previewState,
    additionalFileInfo: {
      ...messageItem.data,
      mbSize: bytesToMegabytes(messageItem?.data?.size),
      renderedType: messageItem?.data?.name?.split('.')[1].toLocaleUpperCase(),
    },
  };
  return information;
}
