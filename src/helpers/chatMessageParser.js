const replyMessageTypes = {
  text: 3,
  sticker: 4,
  image: 5,
  video: 6,
  audio: 7,
  file: 8,
  location: 9,
  notify: 10,
  deleted: 11,
  history: 12,
  WaitForResponse: 13,
  UnknownMessage: 14,
};

const videoAsFileTypes = [
  'video/webm',
  'video/x-flv',
  'video/mkv',
  'video/matroska',
  'video/x-matroska',
  'video/divxplus',
];

const videoAsAudioTypes = [
  'video/mpeg',
  'video/x-mpeg',
  'video/mkv',
  'video/matroska',
  'video/x-matroska',
  'video/divxplus',
];

export const detectTypeOfMessage = (information) => {
  const { mtype, data } = information;
  const messageType = mtype.split('.')[1];

  switch (messageType) {
    case 'cpy':
      return 'cpy';
    case 'txt':
    case 'jnd':
    case 'edt':
      return 'text';
    case 'img':
    case 'vid':
    case 'aud':
    case 'doc':
      return detectFileTypeForDownloading(data.type);
    case 'mis':
      return 'missCall';
    default:
      return null;
  }
};
export const detectFileTypeForSendingMessage = (type, forceFileType) => {
  const splitType = forceFileType || type.split('/')[0];
  switch (splitType) {
    case 'image':
      return {
        type: 'oto.img',
        notificationTitle: 'Image Message',
      };
    case 'video':
      return {
        type: 'oto.vid',
        notificationTitle: 'Video Message',
      };
    case 'audio':
      return {
        type: 'oto.aud',
        notificationTitle: 'Audio Message',
      };
    default:
      return {
        type: 'oto.doc',
        notificationTitle: 'File Message',
      };
  }
};

export const detectFileTypeForDownloading = (type) => {
  const splitType = type.split('/')[0];
  if (splitType === 'image' && !type.includes('svg')) return 'image';
  if (
    splitType === 'video' &&
    !videoAsFileTypes.includes(type) &&
    !videoAsAudioTypes.includes(type)
  )
    return 'video';
  if (splitType === 'audio' || videoAsAudioTypes.includes(type)) return 'audio';
  return 'file';
};
export const detectReplyMessageType = (data) => {
  if (typeof data === 'number' || !isNaN(Number(data))) {
    const inputNumber = Number(data);
    const messageType = Object.keys(replyMessageTypes).find(
      (key) => replyMessageTypes[key] === inputNumber,
    );
    return messageType ? messageType : 'UnknownMessage';
  } else if (typeof data === 'string') {
    const messageType = replyMessageTypes[data];
    return messageType ? messageType : 'UnknownMessage';
  } else {
    return 'Unknown type';
  }
};

export const detectUserAction = (information) => {
  if (information.mtype.split['.'][1] !== 'cst') return null;
  const { stt } = information.data;
  switch (stt) {
    case 'tps':
      return 'typingStart';
    case 'tpt':
      return 'typingStop';
    case 'rcs':
      return 'recordingAudioStart';
    case 'rct':
      return 'recordingAudioStop';
    case 'sfs':
      return 'sendingFileStart';
    case 'sft':
      return 'sendingFileStop';
    default:
      return null;
  }
};

export function calculateImageAndVideoAcceptRatio(imageDimension) {
  const [imageWidth, imageHeight] = imageDimension.split('x');
  const originalWidth = imageWidth;
  const originalHeight = imageHeight;
  const maxWidth = (innerWidth * 80) / 100; // Set the maximum width for rendering
  const maxHeight = (innerHeight * 40) / 100; // Set the maximum height for rendering
  const aspectRatio = originalWidth / originalHeight;
  let renderedWidth, renderedHeight;

  if (originalWidth > maxWidth || originalHeight > maxHeight) {
    if (maxWidth / aspectRatio < maxHeight) {
      renderedWidth = maxWidth;
      renderedHeight = renderedWidth / aspectRatio;
    } else {
      renderedHeight = maxHeight;
      renderedWidth = renderedHeight * aspectRatio;
    }
  } else {
    // If the image is smaller than the maximum dimensions, keep its original size
    renderedWidth = originalWidth;
    renderedHeight = originalHeight;
  }
  return {
    renderedWidth,
    renderedHeight,
  };
}
