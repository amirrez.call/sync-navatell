import { useWebWorkerFn } from '@vueuse/core';

export const getChatRoomList = async (token) => {
  const baseUrl =
    import.meta.env['VITE_APP_BASE_URL'] +
    import.meta.env['VITE_APP_GROUP_PATH'] +
    '/get-list';

  const { workerFn } = useWebWorkerFn(async (baseUrl, token) => {
    const response = fetch(baseUrl, {
      method: 'GET', // Specify the HTTP method (GET in this case)
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((res) => res.json());
    const result = await response;
    return result.groups || [];
  });
  const result = await workerFn(baseUrl, token);
  return result;
};

export const prepardGroupData = async (token) => {
  const retrivalBaseUrl =
    import.meta.env['VITE_APP_BASE_URL'] +
    import.meta.env['VITE_APP_MSG_GORETRIEVAL'] +
    '/groupdetails/';
  const { workerFn } = useWebWorkerFn((baseUrl, listData, token) => {
    const promises = listData.map((item) => sendAPIRequest(item, token));
    function sendAPIRequest(groupData, token) {
      const url = baseUrl + groupData.group_id;
      return fetch(url, {
        method: 'GET', // Specify the HTTP method (GET in this case)
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          return {
            ...data,
            ...groupData,
          };
        });
    }
    return Promise.all(promises)
      .then((data) => {
        return data;
      })
      .catch(() => {
        return [];
      });
  });
  const groupList = await getChatRoomList(token);
  const result = await workerFn(retrivalBaseUrl, groupList, token);
  return result;
};
