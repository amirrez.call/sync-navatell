import axios from './axiosConfig';

export const loadCurrentGroupMessage = (groupId, messagesCount) => {
  return axios.get(`grp/last/${groupId}/${messagesCount}`).then((response) => {
    return response;
  });
};

export const loadGroupMessagesById = (groupId, messagesCount) => {
  return axios.get(`grp/last/${groupId}/${messagesCount}`).then((response) => {
    return response;
  });
};
