import { defineStore } from 'pinia';
import { connector } from '@/api/rtm/index.js';
import { useUserStore } from '@/store/user/user.js';
import { useOtoStore } from '@/store/chats/otoChat.js';
import { useGroupChat } from '@/store/chats/groupChat.js';
import { useOverallChatsStore } from '@/store/chats/overall.js';
import { useChatInput } from '../chats/chatInput';
import { useFileManagerStore } from '@/store/fileManager/fileManager';
import { useContactsStore } from '@/store/contacts/contacts.js';
import { useNestedModals } from '@/store/nestedModals/nestedModals';
import { useVideoCallStore } from '@/store/videoCall/videoCall.js';
import {
  handlerCurrenGroupMessagesMaker,
  handlerSingleMessagesMaker,
  handlerImageMessagesMaker,
  handlerStickerMessagesMaker,
} from '@/helpers/parser.js';
import { loadCurrentGroupMessage } from '@/api/groups/groupMessages/index.js';
//should be changed

export const useRTMStore = defineStore('RTMStore', {
  state: () => ({
    websokectInstance: null,
    tryToConnect: 0,
  }),
  actions: {
    connect() {
      const userStore = useUserStore();
      if (this.websokectInstance != null) {
        this.websokectInstance.close(4014, 'finished');
      }
      //Try to create a websoket instance
      this.websokectInstance = connector(userStore.token);
      //Recived Data
      this.websokectInstance.onmessage = recivedMessageHandler;
      this.websokectInstance.onopen = (event) => {};
      this.websokectInstance.onerror = () => {
        console.log('RTM Disconnect');
        if (this.tryToConnect > 20) {
          // handel rtm connection filed error
          return;
        }
        setTimeout(() => {
          this.connect();
          this.tryToConnect++;
        }, 1000);
      };
    },
  },
  persist: false,
});
function recivedMessageHandler(event) {
  //Here Every Message Recived
  const messageData = JSON.parse(event.data);
  if (messageData.mtype.startsWith('oto')) {
    const otoStore = useOtoStore();
    otoStore.received(messageData);
  } else if (messageData.mtype.startsWith('grp')) {
    grpHandler(event);
    // groupChatStore.updateDataFromRtm(messageData);
  } else if (messageData.mtype === 'call' && messageData.data.body === 'groupcall') {
    const videoCallStore = useVideoCallStore();
    videoCallStore.gettingVideoCall(messageData);
  }
}

const grpHandler = async (event) => {
  const fileManagerStore = useFileManagerStore();
  const userStore = useUserStore();
  const contactsStore = useContactsStore();
  const chatInputStore = useChatInput();
  const getNavaPhoneUsers = contactsStore.contacts.navaphoneUsers;
  const overallChatsStore = useOverallChatsStore();
  const groupChatStore = useGroupChat();
  const nestedModalsStore = useNestedModals();
  const responseFromSocket = JSON.parse(event.data);

  if (responseFromSocket.mtype.startsWith('grp')) {
    console.log(responseFromSocket);
    if (groupChatStore.inGroupChatRoom) {
      if (responseFromSocket.mtype == 'grp.rmv') {
        if (responseFromSocket.data.uuid == userStore.userId) {
          nestedModalsStore.leaveGroupStatus();
          await overallChatsStore.getChatList();
        }
      }
      if (groupChatStore.currentGroup.group_id == responseFromSocket.to) {
        chatInputStore.updateLastMessageSentId(responseFromSocket.data.id);
        if (userStore.userId != responseFromSocket.from) {
          switch (responseFromSocket.mtype) {
            case 'grp.cfg.avt':
              const downloadImageFile =
                await fileManagerStore.handlerForGettingFile(
                  responseFromSocket.data.value,
                  'image',
                  false,
                );
              groupChatStore.currentGroup.avatar = downloadImageFile.filePath;
              break;
            case 'grp.cfg.tit':
              groupChatStore.currentGroup.title = responseFromSocket.data.value;
              break;
            case 'grp.add':
              await groupChatStore.updateGroupMemberList(responseFromSocket.to);
              break;
            case 'grp.rmv':
              await groupChatStore.updateGroupMemberList(responseFromSocket.to);
              break;
            case 'grp.adm.add':
              await groupChatStore.updateGroupMemberList(responseFromSocket.to);
              break;
            case 'grp.adm.rmv':
              await groupChatStore.updateGroupMemberList(responseFromSocket.to);
              break;
            case 'grp.txt':
              const newMessage = await handlerSingleMessagesMaker(
                groupChatStore.currentGroup.members,
                responseFromSocket,
              );
              groupChatStore.currentGroup.messages.push(newMessage);
              break;
            case 'grp.edt':
              setTimeout(async () => {
                const response = await loadCurrentGroupMessage(
                  responseFromSocket.to,
                  30,
                );
                const messages = await handlerCurrenGroupMessagesMaker(
                  getNavaPhoneUsers,
                  response.data,
                );
                groupChatStore.currentGroup.messages = messages;
              }, 3000);
              break;
            case 'grp.del':
              groupChatStore.deleteMessageFromCurrentGroupClient(
                responseFromSocket.data.guid,
              );
              break;
            case 'grp.doc':
              const newFile = await handlerSingleMessagesMaker(
                groupChatStore.currentGroup.members,
                responseFromSocket,
              );
              groupChatStore.currentGroup.messages.push(newFile);
              break;
            case 'grp.aud':
              const newVoice = await handlerSingleMessagesMaker(
                groupChatStore.currentGroup.members,
                responseFromSocket,
              );
              groupChatStore.currentGroup.messages.push(newVoice);
              break;
            case 'grp.img':
              const newImageMessage = await handlerImageMessagesMaker(
                getNavaPhoneUsers,
                responseFromSocket,
              );
              groupChatStore.currentGroup.messages.push(newImageMessage);
              break;
            case 'grp.stk':
              const newSticker = await handlerStickerMessagesMaker(
                getNavaPhoneUsers,
                responseFromSocket,
              );
              groupChatStore.currentGroup.messages.push(newSticker);
              break;
            case 'grp.mst':
              setTimeout(() => {
                const targetMessageForSeen =
                  groupChatStore.currentGroup.messages.find((message) => {
                    return message.guid == responseFromSocket.data.guid;
                  });
                targetMessageForSeen.deliverstatus = 1;
              }, 1000);
              break;
          }
        } else {
          await overallChatsStore.updateListFromGroupData(responseFromSocket);
          switch (responseFromSocket.mtype) {
            case 'grp.edt':
              setTimeout(async () => {
                const response = await loadCurrentGroupMessage(
                  responseFromSocket.to,
                  10,
                );
                const messages = await handlerCurrenGroupMessagesMaker(
                  getNavaPhoneUsers,
                  response.data,
                );
                groupChatStore.currentGroup.messages = messages;
              }, 3000);
              break;

            case 'grp.img':
              const newMessage = await handlerImageMessagesMaker(
                getNavaPhoneUsers,
                responseFromSocket,
              );
              const targetMessage = groupChatStore.currentGroup.messages.find(
                (message) => {
                  if (message.id) {
                    return message.id == newMessage.body.data.id;
                  }
                },
              );
              const detectIndex =
                groupChatStore.currentGroup.messages.indexOf(targetMessage);
              groupChatStore.currentGroup.messages[detectIndex] = newMessage;
              break;

            case 'grp.stk':
              const newSticker = await handlerStickerMessagesMaker(
                getNavaPhoneUsers,
                responseFromSocket,
              );
              const targetSticker = groupChatStore.currentGroup.messages.find(
                (message) => {
                  if (message.id) {
                    return message.id == newSticker.body.data.id;
                  }
                },
              );
              const detectSticker =
                groupChatStore.currentGroup.messages.indexOf(targetSticker);
              groupChatStore.currentGroup.messages[detectSticker] = newSticker;
              break;

            case 'grp.mst':
              setTimeout(() => {
                const targetMessageForSeen =
                  groupChatStore.currentGroup.messages.find((message) => {
                    return message.guid == responseFromSocket.data.guid;
                  });
                targetMessageForSeen.deliverstatus = 1;
              }, 500);
              break;

            default:
              setTimeout(async () => {
                const newMessage = await handlerSingleMessagesMaker(
                  groupChatStore.currentGroup.members,
                  responseFromSocket,
                );
                const targetMessage = groupChatStore.currentGroup.messages.find(
                  (message) => {
                    if (message.id) {
                      return message.id == newMessage.body.data.id;
                    }
                  },
                );
                const detectIndex =
                  groupChatStore.currentGroup.messages.indexOf(targetMessage);
                groupChatStore.currentGroup.messages[detectIndex] = newMessage;
              }, 100);
              break;
          }
        }
      }
    } else {
      await overallChatsStore.updateListFromGroupData(responseFromSocket);
    }
  }
};
