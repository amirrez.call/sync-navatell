import { defineStore } from 'pinia';

export const useThemeStore = defineStore('ThemeStore', {
  state: () => ({
    autoDetected: null, // fa,en
    userSelected: null,
  }),
  getters: {
    getThemeIsDark(state) {
      if (state.userSelected) {
        return state.userSelected == 'dark' ? true : false;
      }
      return state.autoDetected == 'dark' ? true : false;
    },
  },
  actions: {
    setAutoDetectedTheme() {
      if (
        window.matchMedia &&
        window.matchMedia('(prefers-color-scheme: dark)').matches
      ) {
        this.autoDetected = 'dark';
        // Dark mode is preferred by the user
      } else {
        // Light mode is preferred by the user
        this.autoDetected = 'light';
      }
    },
    setUserSelectedThem(value) {
      this.userSelected = value;
    },
  },
  persist: true,
});
