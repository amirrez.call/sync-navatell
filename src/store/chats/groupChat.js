import { defineStore } from 'pinia';
import { getFile } from '@/helpers/parser.js';
import { MembersParser } from '@/helpers/parser.js';
import { useOverallChatsStore } from './overall.js';
import {
  parserForAddMemberToCurrentGroup,
  handlerCurrenGroupMessagesMaker,
  handleMessagesMakerAfterScroll,
  sennMessageParser,
} from '@/helpers/parser.js';
import { useUserStore } from '@/store/user/user';
import { videoCallState } from '@/api/video-call/index.js';

import {
  getGroupTitle,
  getGroupAvatar,
  getGroupMembers,
  editTitleGroup,
  updateAvatar,
  addMemberToCurrentGroup,
  removeMemberFromCurrentGroup,
  addAdminRoleToMemberFromCurrentGroup,
  removeAdminRoleToMemberFromCurrentGroup,
  leaveFromCurrentGroup,
  destroyCurrentGroup,
  deleteMessage,
  seenMessage,
} from '@/api/groups/index.js';
import { loadCurrentGroupMessage } from '@/api/groups/groupMessages/index.js';
import { useContactsStore } from '@/store/contacts/contacts.js';

export const useGroupChat = defineStore('groupChatStore', {
  state: () => ({
    dataFromRtm: [],
    lastCreatedGroup: {},
    memberUidInCurrentGroup: [],
    filterByCurrentGroupMember: [],
    memberForAddToCurrentGroup: [],
    inGroupChatRoom: false,
    currentGroup: {
      group_id: '',
      title: '',
      avatar: '',
      members: [],
      messages: [],
      activeCall: false,
    },
    loading: false,
    infiniteLoading: false,
    currentGroupMessageLength: 0,
  }),

  actions: {
    deleteMessageFromCurrentGroupClient(id) {
      this.currentGroup.messages = this.currentGroup.messages.filter(
        (message) => message.guid != id,
      );
    },

    handleReplaceItemForEdit(index, newMessage) {
      this.currentGroup.messages[index] = newMessage;
    },

    updateCurrentGroupMessageLength(param) {
      this.currentGroupMessageLength = param;
    },

    clearCurrentGroup() {
      this.currentGroup.group_id = '';
      this.currentGroup.avatar = '';
      this.currentGroup.title = '';
      this.currentGroup.members = [];
      this.currentGroup.messages = [];
    },

    updateCurrentGroupMessageInClient(param) {
      this.currentGroup.messages.push(param);
    },

    updateInChatRoomStatus(param) {
      this.inGroupChatRoom = param;
    },

    updateDataFromRtm(param) {
      this.dataFromRtm.push(param);
    },

    changeLaodingstatus(status) {
      this.loading = status;
    },

    async filterByCurrentGroupMember() {
      const contactsStore = useContactsStore();
      const userStore = useUserStore();
      const navaPhoneConatcts = contactsStore.contacts.navaphoneUsers;
      const currentGroupMembers = this.currentGroup.members;
      const result = await parserForAddMemberToCurrentGroup(
        navaPhoneConatcts,
        currentGroupMembers,
      );
      const array = [];
      for (let i of result) {
        if (i.contact_username) {
          const obj = { role: '', uuid: i.contact_username };
          if (obj.uuid != userStore.userId) {
            array.push(obj);
          }
        }
      }
      const members = await MembersParser(navaPhoneConatcts, array);
      this.memberForAddToCurrentGroup = members;
    },

    async getGroupTitle(groupId) {
      const result = await getGroupTitle(groupId);
      this.currentGroup.group_id = result.group_id;
      this.currentGroup.title = result.value;
    },

    async getGroupAvatarFileId(groupId) {
      const result = await getGroupAvatar(groupId);
      if (result.value) {
        const fileId = result.value.split('.');
        const fileIdResult = fileId[0] ? fileId[0] : result.value;
        return fileIdResult;
      }
    },

    async getGroupAvatar(groupId) {
      const userStore = useUserStore();
      const result = await getGroupAvatar(groupId);
      if (result.value) {
        if (result.value == 'capture_photo.png') {
          this.currentGroup.avatar = '';
        } else {
          const fileId = result.value.split('.');
          const fileIdResult = fileId[0] ? fileId[0] : result.value;
          const blobFile = await getFile(
            fileIdResult,
            userStore.token,
            'image',
          );
          this.currentGroup.avatar = blobFile.filePath;
        }
      }
    },

    async getGroupMembersUid(groupId) {
      const result = await getGroupMembers(groupId);
      this.memberUidInCurrentGroup = result;
    },

    async addMemberToCurrentGroup() {
      const overallChatsStore = useOverallChatsStore();
      const result = overallChatsStore.groupMembersClient.map((item) => {
        return item.contact_username;
      });
      addMemberToCurrentGroup(this.currentGroup.group_id, result).then(
        async (res) => {
          await this.updateGroupMemberList(this.currentGroup.group_id);
          await this.loadedCurrentGroupMessages(this.currentGroup.group_id, 50);
          overallChatsStore.clearGroupMembersClient();
        },
      );
    },

    async removeMemberFromCurrentGroup(memberID) {
      await removeMemberFromCurrentGroup(this.currentGroup.group_id, memberID);
      await this.updateGroupMemberList(this.currentGroup.group_id);
    },

    async addAdminRoleToMemberFromCurrentGroup(memberID) {
      await addAdminRoleToMemberFromCurrentGroup(
        this.currentGroup.group_id,
        memberID,
      );
      await this.updateGroupMemberList(this.currentGroup.group_id);
    },

    async removeAdminRoleToMemberFromCurrentGroup(memberID) {
      await removeAdminRoleToMemberFromCurrentGroup(
        this.currentGroup.group_id,
        memberID,
      );
      await this.updateGroupMemberList(this.currentGroup.group_id);
    },

    async leaveFromCurrentGroup(role) {
      const overallChatsStore = useOverallChatsStore();
      // const nestedModalsStore = useNestedModals();
      if (role === 'owner') {
        // nestedModalsStore.leaveGroupStatus();
        this.destroyCurrentGroup(this.currentGroup.group_id);
        await leaveFromCurrentGroup(this.currentGroup.group_id);
        await overallChatsStore.getChatList();
      } else {
        await leaveFromCurrentGroup(this.currentGroup.group_id);
        await overallChatsStore.getChatList();
      }
    },

    async destroyCurrentGroup() {
      await destroyCurrentGroup(this.currentGroup.group_id);
    },

    async editTitleGroup(value) {
      await editTitleGroup(this.currentGroup.group_id, value);
      this.getGroupTitle(this.currentGroup.group_id);
      await this.loadedCurrentGroupMessages(this.currentGroup.group_id, 50);
    },

    async updateAvatar(value) {
      await updateAvatar(this.currentGroup.group_id, value);
    },

    async getGroupMembers() {
      const contactsStore = useContactsStore();
      const getNavaPhoneUsers = contactsStore.contacts.navaphoneUsers;
      this.currentGroup.members = await MembersParser(
        getNavaPhoneUsers,
        this.memberUidInCurrentGroup,
      );
    },

    async updateGroupMemberList(groupId) {
      const contactsStore = useContactsStore();
      const getNavaPhoneUsers = contactsStore.contacts.navaphoneUsers;
      await this.getGroupMembersUid(groupId);
      const newList = await MembersParser(
        getNavaPhoneUsers,
        this.memberUidInCurrentGroup,
      );
      const localStorageData = localStorage.getItem('groups');
      const parseLocalStorageData = JSON.parse(localStorageData);
      const currentGroupData = parseLocalStorageData.find((g) => {
        return g.groupId == this.currentGroup.group_id;
      });
      currentGroupData.members = [];
      currentGroupData.members = newList;
      const currentGroupIndex = parseLocalStorageData.indexOf(currentGroupData);
      parseLocalStorageData[currentGroupIndex] = currentGroupData;
      const dataToJosn = JSON.stringify(parseLocalStorageData);
      localStorage.setItem('groups', dataToJosn);
      this.currentGroup.members = newList;
    },

    async loadedCurrentGroupMessages(groupId, count) {
      const localStorageData = localStorage.getItem('groups');
      const parseLocalStorageData = JSON.parse(localStorageData);
      const currentGroupData = parseLocalStorageData.find((g) => {
        return g.groupId == this.currentGroup.group_id;
      });
      this.changeLaodingstatus(true);
      if (!currentGroupData) {
        await this.getGroupMembers();
        const groupData = {
          groupId: this.currentGroup.group_id,
          members: this.currentGroup.members,
        };
        parseLocalStorageData.push(groupData);
        const dataToJosn = JSON.stringify(parseLocalStorageData);
        localStorage.setItem('groups', dataToJosn);
        const response = await loadCurrentGroupMessage(groupId, count);
        this.currentGroup.messages = await handlerCurrenGroupMessagesMaker(
          this.currentGroup.members,
          response.data,
        );
      } else {
        this.currentGroup.members = currentGroupData.members;
        const response = await loadCurrentGroupMessage(groupId, count);
        this.currentGroup.messages = await handlerCurrenGroupMessagesMaker(
          this.currentGroup.members,
          response.data,
        );
      }
      this.changeLaodingstatus(false);
    },

    async loadMessagesAfterScroll(groupId, count) {
      if (this.currentGroup.messages[0].mtype != 'grp.crt') {
        this.infiniteLoading = true;
        const contactsStore = useContactsStore();
        // const getNavaPhoneUsers = contactsStore.contacts.navaphoneUsers;
        const response = await loadCurrentGroupMessage(groupId, count);
        const messages = response.data.messages.reverse().slice(0, 20);
        const newMessages = await handleMessagesMakerAfterScroll(
          this.currentGroup.members,
          messages,
        );
        this.currentGroup.messages.unshift(...newMessages);
        this.infiniteLoading = false;
      }
    },

    async deletedMessage(param) {
      await deleteMessage(param).then((res) => {
        this.deleteMessageFromCurrentGroupClient(param.guid);
      });
    },

    async editedMessage(param) {
      await editMessage(param);
    },

    async seenedMessage(param) {
      const parse = await sennMessageParser(param);
      await seenMessage(parse);
    },

    async checkActiveCall() {
      const groupId = this.currentGroup.group_id;
      const isBusy = await videoCallState(groupId);
      if (isBusy) {
        this.currentGroup.activeCall = true;
      }
    },
  },
});
