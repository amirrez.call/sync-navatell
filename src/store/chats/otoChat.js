import { defineStore } from 'pinia';
import { sendMessage, getPreviouslyMessages } from '@/api/OTO/index.js';
import { useFileManagerStore } from '@/store/fileManager/fileManager.js';
import { v1 as idGenerator } from 'uuid';
import { chatsParser, generateDataModelInChat } from '@/helpers/otoParser.js';
import { useUserStore } from '@/store/user/user.js';
import { useObjectUrl } from '@vueuse/core';
import { useOverallChatsStore } from '@/store/chats/overall.js';
import {
  detectTypeOfMessage,
  detectFileTypeForSendingMessage,
} from '@/helpers/chatMessageParser.js';

export const useOtoStore = defineStore('OtoStore', {
  state: () => ({
    chatList: {},
    currentChatId: '',
    scrollerFunction: null,
    showLoading: false,
  }),
  getters: {
    getCurrentChat(state) {
      if (state.currentChatId) {
        return state.chatList[state.currentChatId];
      }
      return [];
    },
  },
  actions: {
    handlerForGettingMessages(
      chatId,
      lastMessageId,
      lastSeenMessageId,
      source,
    ) {
      if (!this.chatList[chatId]) {
        this.showLoading = true;
      }
      return this.getPrevChatMessages(chatId, lastMessageId, 100, source);
    },
    getPrevChatMessages(chatId, targetMessageId, count, source) {
      return getPreviouslyMessages(chatId, targetMessageId, count).then(
        async (result) => {
          const userStore = useUserStore();
          const userId = userStore.userId;
          const parsedList = await chatsParser(result, userId, source);
          this.chatList[this.currentChatId] = parsedList;
          this.showLoading = false;
          return true;
        },
      );
    },
    send({
      text,
      title,
      files,
      textDirectionIsRtl,
      repliedData,
      forceFileType,
      webChatData,
      chatId,
    }) {
      const userStore = useUserStore();
      const userId = userStore.userId;
      const currentChatId = chatId || this.currentChatId;
      const targetOtherSideChatId = userId;
      // This functionality detect is message is text or file
      // if you pass files parameter to this function it detects message as file message

      // This condition detect message is text message
      if (!files || files.length == 0) {
        const information = {
          to: currentChatId, // it`s equal to userId
          id: idGenerator(),
          message: text,
          mtype: 'oto.txt',
          title: title,
          data: {
            chatId: targetOtherSideChatId,
            senderTargetChatId: currentChatId,
            ...repliedData,
            ...webChatData,
          },
        };
        this.addManuallyChatItemToChatList(
          information,
          currentChatId,
          textDirectionIsRtl,
          'text',
        );
        return sendMessage(information);
      }

      // Here is File Message part functionality
      // first convert object type files to array
      const filesArray = Object.values(files);
      // upload each file to server by fileManager store
      const fileManagerStore = useFileManagerStore();

      filesArray.forEach((file, index) => {
        const additionalFileInfo = detectFileTypeForSendingMessage(
          file.type,
          forceFileType,
        );
        const previewImage = useObjectUrl(file).value;
        let information = {
          to: currentChatId, // its same userId (uuid)
          id: idGenerator(),
          message: additionalFileInfo.notificationTitle,
          mtype: additionalFileInfo.type, // oto.img || oto.vid || oto.aud || oto.doc
          title: title, // Nick Name of user that send message
          // for sending file as a message this property is needed
          data: {
            chatId: targetOtherSideChatId,
            senderTargetChatId: currentChatId,
            fileId: Math.random(),
            description: '',
            md5: 'cudFile.md5',
            name: file.name,
            size: file.size.toString(),
            type: forceFileType ? 'oto.doc' : file.type,
            dim: file?.dimension
              ? `${file.dimension.width}x${file.dimension.height}`
              : null,
            duration: +file.duration,
            ...repliedData,
            ...webChatData,
          },
          previewBlob: previewImage,
          status: 'sending',
        };

        this.addManuallyChatItemToChatList(
          information,
          currentChatId,
          textDirectionIsRtl,
          additionalFileInfo.type,
        );
        return fileManagerStore.uploadFile(file).then((fileId) => {
          if (text != null && text != '' && filesArray.length - 1 == index) {
            information.data.description = text;
          }
          information['data'].fileId = fileId;
          return sendMessage(information);
        });
      });
    },
    addManuallyChatItemToChatList(
      messageItem,
      chatId,
      textDirectionIsRtl = true,
      type = 'file',
    ) {
      const information = generateDataModelInChat(
        messageItem,
        true,
        textDirectionIsRtl,
        type,
      );
      const keyChat = chatId;
      const targetChat = this.chatList[keyChat];
      if (targetChat) {
        this.chatList[keyChat].push(information);
      } else {
        this.chatList[keyChat] = [information];
      }
      requestAnimationFrame(() => {
        this.scrollerFunction();
      });
    },
    received(rtmData) {
      const typeOfMessageData = detectTypeOfMessage(rtmData);
      const currentChatId = this.currentChatId;
      const overallStore = useOverallChatsStore();
      overallStore.updateList(rtmData);
      if (rtmData.from == rtmData.data.senderTargetChatId) {
        return this.updateSeenMessagesStatus(rtmData);
      }
      if (
        rtmData.data.body == 'voipcall' ||
        rtmData.mtype == 'oto.mis' ||
        rtmData.data.mtype == 'oto.mis'
      )
        return;
      if (rtmData.mtype == 'oto.mst' && rtmData.data.preventSeenRequest) {
        return;
      }
      if (rtmData.mtype != 'oto.cpy' && currentChatId != rtmData.data.chatId) {
        return;
      } // its mean chat is not open
      if (rtmData.mtype == 'oto.cst') {
        return;
      }
      if (rtmData.mtype == 'oto.cpy' && rtmData.data.mtype == 'oto.mst') {
        return;
      } // here you send seen request and get it by rtm
      if (typeOfMessageData == 'cpy' && rtmData.data.mtype != 'oto.mst') {
        if (rtmData.data.body == 'voipcall' || rtmData.data.mtype == 'oto.mis')
          return;
        this.updateStateOfMessage(rtmData);
        return;
      }
      if (rtmData.mtype == 'oto.mst' && !rtmData.data.preventSeenRequest) {
        this.updateSeenMessagesStatus(rtmData);
        return;
      }

      const information = generateDataModelInChat(rtmData, false);
      const keyChat = rtmData.from;
      const id = rtmData.data.guid;
      const targetChat = this.chatList[keyChat];
      if (targetChat) {
        this.chatList[keyChat].push(information);
        requestAnimationFrame(() => {
          if (this.scrollerFunction && this.currentChatId) {
            this.scrollerFunction();
            this.sendSeenRequestOfMessage(currentChatId, id);
          }
        });
        return;
      }
      this.chatList[keyChat] = [
        {
          ...information,
        },
      ];
      if (this.currentChatId) {
        this.sendSeenRequestOfMessage(currentChatId, id);
      }
    },
    sendSeenRequestOfMessage(chatId, messageGuid, preventSeenRequest = null) {
      const userStore = useUserStore();
      const userId = userStore.userId;
      if (chatId == userId) return;
      const information = {
        to: chatId, // it`s equal to userId
        id: idGenerator(),
        mtype: 'oto.mst',
        data: {
          stt: 5,
          guid: messageGuid,
          chatId: userId,
          senderTargetChatId: chatId,
          preventSeenRequest,
        },
      };
      return sendMessage(information);
    },
    updateStateOfMessage(messageData) {
      const targetChatId = this.currentChatId;
      const messageId = messageData.data.id;
      const targetChatList = this.chatList[targetChatId];
      if (targetChatList) {
        const targetMessage = targetChatList.find((e) => e.id == messageId);
        targetMessage.status = 'sent';
      }
    },
    updateSeenMessagesStatus(messageData) {
      const userStore = useUserStore();
      if (messageData.from == userStore.userId) return;
      const targetChatId = this.currentChatId;
      if (!targetChatId || targetChatId == '') return;
      const chatList = this.chatList[targetChatId];
      const filtredList = chatList.filter((e) => e.status != 'seen');
      filtredList.forEach((e) => {
        e.status = 'seen';
        this.sendSeenRequestOfMessage(
          targetChatId,
          e.additionalFileInfo.guid,
          true,
        );
      });
    },
    setScrollerFucntion(scrollerFunction) {
      this.scrollerFunction = scrollerFunction;
    },
  },
  persist: {
    paths: ['chatList', 'currentChatId'],
  },
});
