import { defineStore } from 'pinia';
import { getChatRoomList } from '@/api/OTO/index.js';
import { prepardGroupData } from '@/helpers/grpParser.js';
import { useUserStore } from '@/store/user/user.js';
import { createGroup } from '@/api/groups/index';
import { useGroupChat } from '@/store/chats/groupChat';
import { useContactsStore } from '@/store/contacts/contacts.js';
import {
  chatListParser,
  orderListHandler,
} from '@/helpers/overallChatListParser.js';
import { getUnsavedUsersInformation } from '@/helpers/contactsParser.js';
import { detectTextDirection } from '@/helpers/textFormatter.js';
export const useOverallChatsStore = defineStore('OverallChatsStore', {
  state: () => ({
    chatsList: [],
    currentChat: '',
    groupName: '',
    groupMembersClient: [],
    groupMembersIdForServer: [],
    avatar: '',
    avatarBlob: '',
    draftLastCreatedGroupName: '',
  }),
  getters: {
    getListChats() {
      if (!this.chatsList) return [];
      return this.chatsList;
    },
    getForwardedDataList() {
      const contactStore = useContactsStore();
      let chatList = this.getListChats;
      const savedMessageChatIndex = chatList.findIndex((e) => e.isMySelf);
      if (savedMessageChatIndex != -1) {
        chatList.splice(savedMessageChatIndex, 1);
      }
      const contactsList = contactStore.contacts.navaphoneUsers;
      let prepardList = [];
      if (chatList.length > 0) {
        prepardList[0] = recentMessagesTitle;
        prepardList.push(...chatList);
      }
      if (contactsList.length > 0) {
        prepardList.push(recentMessagesTitleContacts);
        prepardList.push(...contactsList);
      }
      return prepardList;
    },
  },
  actions: {
    updateDraftLastCreatedGroupName(param) {
      this.draftLastCreatedGroupName = param;
    },
    updateAvatarBlob(param) {
      this.avatarBlob = param;
    },

    updateAvatar(param) {
      this.avatar = param;
    },

    addMemberToGroup(member) {
      const membersID = this.groupMembersClient
        .map((item) => item.id)
        .includes(member.id);
      if (!membersID) {
        this.groupMembersClient.push(member);
      } else {
        this.groupMembersClient = this.groupMembersClient.filter(
          (item) => item.id != member.id,
        );
      }
    },

    clearGroupMembersClient() {
      this.groupMembersClient = [];
    },

    updateNameGroup(name) {
      this.groupName = name;
    },

    handlerFilterMembersId() {
      const MembersID = this.groupMembersClient.map(
        (member) => member.contact_username,
      );
      this.groupMembersIdForServer = MembersID;
    },

    async getChatList() {
      const userStore = useUserStore();
      const concatcStore = useContactsStore();
      const otosData = await getChatRoomList();
      const groupsData = await prepardGroupData(userStore.token);
      const parserResult = await chatListParser(
        {
          serverChatList: groupsData.concat(otosData),
          storageChatList: this.chatsList || [],
          storageNavaUsers: concatcStore.contacts.navaphoneUsers || [],
          storageUnsavedUsers: concatcStore.contacts.unSavedUsers || [],
        },
        userStore.userId,
        userStore.phoneNumber,
      );
      this.chatsList = parserResult.orderedList;
      let unSavedUsers = concatcStore.contacts.unSavedUsers;
      unSavedUsers = unSavedUsers.concat(parserResult.unSavedUsers);
      concatcStore.contacts.unSavedUsers = unSavedUsers;
      return true;
    },
    async updateList(rtmData) {
      // return;
      if (rtmData.data.body == 'voipcall' || rtmData.mtype == 'oto.mis') return;
      if (rtmData.mtype == 'oto.cpy' && rtmData.data.mtype == 'oto.mst') {
        const chatId = rtmData.data.senderTargetChatId;
        const targetChat = this.chatsList.find((e) => e.chatId == chatId) || {};
        targetChat.badge = 0;
        return;
      }
      if (rtmData.mtype == 'oto.cpy' && rtmData.data.mtype != 'oto.mst') {
        /// here should update chat item by yoursleft data
        // console.log(targetChat, 'target Chat in 1111', rtmData);
        const chatId = rtmData.data.senderTargetChatId || rtmData.data.to;
        const targetChat = this.chatsList.find((e) => e.chatId == chatId) || {};
        targetChat.isFromMe = true;
        targetChat.badge = 0;
        targetChat.date = rtmData.data.ts;
        targetChat.lastMessageData = {
          content: rtmData.message,
          type: rtmData.mtype,
          guid: rtmData.data.guid,
        };
        targetChat.lastMessageInfo = rtmData;
        targetChat.lastSeenData = {
          guid: rtmData.data.guid,
          isSeen: false,
        };
        this.chnageChatItemOrderInList(targetChat);

        return;
      }

      if (rtmData.mtype == 'oto.mst') {
        const chatId = rtmData.data.chatId;
        const targetChat = this.chatsList.find((e) => e.chatId == chatId) || {};
        targetChat.lastSeenData.isSeen = true;
        return;
      }
      if (
        ['oto.img', 'oto.txt', 'oto.aud', 'oto.doc', 'oto.vid'].includes(
          rtmData.mtype,
        )
      ) {
        const chatId = rtmData.from;
        const targetChat = this.chatsList.find((e) => e.chatId == chatId) || {};
        targetChat.isFromMe = false;
        targetChat.isMySelf = false;
        targetChat.chatId = chatId;
        // console.log(targetChat, 'target Chat in 22222', rtmData);
        if (!targetChat.badge) {
          targetChat.badge = 0;
          targetChat.badge++;
        } else {
          targetChat.badge++;
        }
        // console.log(targetChat.badge);

        targetChat.date = rtmData.data.ts;
        targetChat.lastMessageData = {
          content: rtmData.message,
          type: rtmData.mtype,
          guid: rtmData.data.guid,
        };
        targetChat.lastMessageInfo = rtmData;
        targetChat.lastSeenData = {
          guid: rtmData.data.guid,
          isSeen: false,
        };
        this.chnageChatItemOrderInList(targetChat);
      }
    },
    async chnageChatItemOrderInList(targetChat) {
      let dateString = targetChat.date.toString();
      if (dateString.includes('UTC')) {
        const parts = dateString.split(' ');
        const datePart = parts[0];
        const timePart = parts[1].substring(0, 8);
        const fullDateString = `${datePart}T${timePart}Z`;
        // Create a Date object using the combined date string
        dateString = new Date(fullDateString);
      }
      targetChat.date = dateString.toString();
      targetChat.timestamp = dateString.getTime();
      const indexInChatList = this.chatsList.findIndex(
        (e) => e.chatId == targetChat.chatId,
      );
      if (indexInChatList == 0) return;
      if (indexInChatList == -1) {
        let userInformation = {};
        let information;
        if (targetChat.lastMessageInfo.data.source) {
          userInformation.name = targetChat.lastMessageInfo.data.nickname;
          information = {
            avatarClass: 'avatar-color-' + (Math.floor(Math.random() * 6) + 1),
            name: targetChat.lastMessageInfo.data.nickname,
            isUnSavedUser: true,
            phoneNumber: null,
            synced: true,
          };
        } else {
          const userStore = useUserStore();
          const contactsStore = useContactsStore();
          userInformation =
            contactsStore.contacts.navaphoneUsers.find(
              (user) => user.contact_username == targetChat.chatId,
            ) ||
            contactsStore.contacts.unSavedUsers.find(
              (user) => user.uuid == targetChat.chatId,
            );
          if (!userInformation) {
            userInformation = await getUnsavedUsersInformation(
              targetChat.chatId,
              userStore.token,
              userStore.sessionToken,
            );
            if (userInformation)
              contactsStore.contacts.unSavedUsers.push(userInformation);
          }
          information = {
            avatarClass: 'avatar-color-' + (Math.floor(Math.random() * 6) + 1),
            name: userInformation.name || userInformation.nickname,
            isUnSavedUser: true,
            phoneNumber:
              userInformation.contact_phone || userInformation.phone_number,
            synced: true,
          };
        }
        information['textDirectionIsRight'] = detectTextDirection(
          information.name,
        );
        targetChat.information = information;
        this.chatsList.splice(indexInChatList, 1);
        this.chatsList.unshift(targetChat);
        return;
      }

      const list = JSON.parse(JSON.stringify(this.chatsList));
      const result = await orderListHandler(list);
      this.chatsList = result;
    },
    async createGroup() {
      this.handlerFilterMembersId();
      const groupInformation = {
        title: this.groupName,
        members: this.groupMembersIdForServer,
        avatar: this.avatar,
      };
      await createGroup(groupInformation).then(async (response) => {
        this.clearGroupMembersClient();
        this.updateAvatarBlob('');
        const groupChatStore = useGroupChat();
        groupChatStore.currentGroup.group_id = response.group_id;
        await this.getChatList();
      });
    },

    setCurrentChatId(id) {
      this.currentChat = id;
    },

    updateGroupChatRoomBadge(groupId) {
      const targetchat = this.chatsList.find((chat) => {
        return chat.chatId == groupId;
      });
      if (targetchat.badge) {
        targetchat.badge = 0;
      }
    },

    async updateListFromGroupData(socketReposnse) {
      if (socketReposnse.mtype.startsWith('grp')) {
        const targetchat = this.chatsList.find((chat) => {
          return chat.chatId == socketReposnse.to;
        });
        const detectInex = this.chatsList.indexOf(targetchat);
        if (detectInex == 0) {
          let newTime = socketReposnse.data.ts;
          let parts = newTime.split(' ');
          let datePart = parts[0];
          let timePart = parts[1].substring(0, 8);
          let fullDateString = `${datePart}T${timePart}Z`;
          newTime = new Date(fullDateString);
          if (targetchat.badge == 0) {
            targetchat.badge++;
          } else {
            targetchat.badge++;
          }
          targetchat.lastMessageData.content = socketReposnse.message;
          targetchat.lastMessageInfo.message = socketReposnse.message;
          targetchat.date = newTime.toString();
          targetchat.timestamp = newTime.getTime();
          if (socketReposnse.mtype == 'grp.cfg.tit') {
            targetchat.groupData.name = socketReposnse.data.value;
          }
        } else if (detectInex == -1) {
          if (socketReposnse.mtype == 'grp.crt') {
            let newTime = socketReposnse.data.ts;
            let parts = newTime.split(' ');
            let datePart = parts[0];
            let timePart = parts[1].substring(0, 8);
            let fullDateString = `${datePart}T${timePart}Z`;
            newTime = new Date(fullDateString);
            const newChat = {
              badge: 0,
              chatId: socketReposnse.to,
              date: newTime.toString(),
              timestamp: newTime.getTime(),
              isFromMe: false,
              lastMessageData: { content: '' },
              lastMessageInfo: { message: '' },
              groupData: {
                avatarClass: 'avatar-color-2',
                groupId: socketReposnse.to,
                name: this.draftLastCreatedGroupName,
              },
              information: {
                avatarClass: 'avatar-color-2',
                name: this.draftLastCreatedGroupName,
              },
            };
            setTimeout(async () => {
              this.chatsList.unshift(newChat);
              const list = JSON.parse(JSON.stringify(this.chatsList));
              const result = await orderListHandler(list);
              this.chatsList = result;
            }, 4000);
          }
        } else {
          if (socketReposnse.mtype == 'grp.cfg.tit') {
            targetchat.groupData.name = socketReposnse.data.value;
          }
          let newTime = socketReposnse.data.ts;
          let parts = newTime.split(' ');
          let datePart = parts[0];
          let timePart = parts[1].substring(0, 8);
          let fullDateString = `${datePart}T${timePart}Z`;
          newTime = new Date(fullDateString);
          if (targetchat.badge == 0) {
            targetchat.badge++;
          } else {
            targetchat.badge++;
          }
          targetchat.lastMessageData.content = socketReposnse.message;
          targetchat.lastMessageInfo.message = socketReposnse.message;
          targetchat.date = newTime.toString();
          targetchat.timestamp = newTime.getTime();
          const list = JSON.parse(JSON.stringify(this.chatsList));
          const result = await orderListHandler(list);
          this.chatsList = result;
        }
      }
    },
  },
  persist: [
    {
      paths: ['chatsList'],
    },
  ],
});

const recentMessagesTitleProxy = {
  category: 'recentMessages',
  id: 'recentMessages',
};
const handler = {
  get: function (target, prop) {
    return target[prop];
  },
  set: function (target, prop, value) {
    target[prop] = value;
  },
};
const recentMessagesTitle = new Proxy(recentMessagesTitleProxy, handler);

const recentMessagesContactsProxy = {
  category: 'recentMessagesContacts',
  id: 'recentMessagesContacts',
};
const recentMessagesTitleContacts = new Proxy(
  recentMessagesContactsProxy,
  handler,
);
