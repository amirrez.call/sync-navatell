import { defineStore } from 'pinia';

export const useNestedModalsDesktop = defineStore('nestedmodalsdesktop', {
  state: () => ({
    coreModal: false,
    groupChatRoom: false,
    profile: false,
    removeContact: false,
    addContact: {
      status: false,
      mode: '',
    },
    contactProfile: false,
  }),

  actions: {
    changeStatusCoreModal(status) {
      this.coreModal = status;
    },
    changeStatusGroupChatRoom(status) {
      this.groupChatRoom = status;
    },
    changeStatusProfile(status) {
      this.profile = status;
    },
    changeStatusRemoveContact(status) {
      this.removeContact = status;
    },
    changeStatusAddContact(status, mode) {
      this.addContact.status = status;
      this.addContact.mode = mode;
    },
    changeStatusContactProfile(status) {
      this.contactProfile = status;
    },
  },
});
