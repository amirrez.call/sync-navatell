import { defineStore } from 'pinia';
import { fileUploader, downloaderFile } from '@/api/file/index.js';
import { getFile } from '@/helpers/parser.js';
import { useUserStore } from '@/store/user/user.js';
import {
  addItemToIndexDB,
  getItemFromIndexDB,
  updateItemInIndexDB,
} from '@/helpers/dbManager';

export const useFileManagerStore = defineStore('FileManagerStore', {
  $id: 'FileManagerStore',
  state: () => ({
    filesInUploding: {
      value: {},
    },
    filesInDownloading: {
      value: {},
    },
    blobFilePathDownloadedList: {},
    usersAvatarBlobList: {},
  }),
  getters: {
    getUploadingPersentage(state) {
      return state.filesInUploding;
    },
  },
  actions: {
    uploadFile(file, key = null, componentPersantageVariable = null) {
      /**
       * key is temp unique key assing in filesInUploading array
       * you can generate that simplay as file.name+Date.now()
       * if key passed to here it means we wanna get percentage of uploading file
       * percentage of uploading file accessable by getUploadingPersentage getter
       * componentPersantageVariable if exist it mean you want assign persantage in to your variable in component
       * note componentPersantageVariable must be = ref({})
       */
      return fileUploader(
        file,
        componentPersantageVariable || this.filesInUploding,
        key,
      );
    },
    downloadFile(fileId, type, percentageNeeded = false) {
      return downloaderFile(
        fileId,
        type,
        percentageNeeded ? filesInDownloading : null,
      );
    },
    downloadFileFromWorker(fileId, type = 'image') {
      const userStore = useUserStore();
      const token = userStore.token;
      return getFile(fileId, token, type)
        .then((filePath) => {
          return filePath;
        })
        .catch(() => {
          return null;
        });
    },
    addToDownloadedList(data, key) {
      this.blobFilePathDownloadedList[key] = data;
    },
    async handlerForGettingFile(fileId, type = 'image', isThumbnail = false) {
      return new Promise(async (resolve, reject) => {
        let target = isThumbnail ? 'thumbnail' : type;
        const item = await getItemFromIndexDB('files', fileId);
        const isSameType = isThumbnail ? isThumbnail : item?.type === type;
        if (item && isSameType) {
          const file = item.mainFile || item.thumbnailFile;
          const data = {
            filePath: URL.createObjectURL(file),
            fileId: fileId,
            key: fileId,
            target,
            itsMain: item.target !== 'thumbnail',
          };
          this.blobFilePathDownloadedList[fileId] = data;

          resolve(data);
        } else {
          // not exist so send a request to getting from server
          return this.downloadFileFromWorker(fileId, target).then(
            async (fileData) => {
              const data = {
                filePath: fileData.filePath,
                fileId: fileId,
                key: fileId,
                target,
                itsMain: !isThumbnail,
                mainFile: isThumbnail ? item?.mainFile : fileData.blob,
                thumbnailFile: isThumbnail
                  ? fileData.blob
                  : item?.thumbnailFile,
                id: fileId
              };

              this.blobFilePathDownloadedList[fileId] = data;

              if (item) {
                await updateItemInIndexDB('files', data);
              } else {
                await addItemToIndexDB('files', data);
              }

              resolve(data);
            },
          );
        }
      });
    },
    async gettingAvatarsHandler(userAvatarFileId, isThumbnail = true) {
      const item = await getItemFromIndexDB('avatars', userAvatarFileId);
      if (item) {
        if (item.thumbnailFile && isThumbnail) {
          this.usersAvatarBlobList[userAvatarFileId] = {
            thumbnailFile: URL.createObjectURL(item.thumbnailFile),
            mainFile: item.mainFile
              ? URL.createObjectURL(item.mainFile)
              : false,
          };
          return;
        }
        if (item.mainFile && !isThumbnail) {
          this.usersAvatarBlobList[userAvatarFileId] = {
            thumbnailFile: item.thumbnailFile
              ? URL.createObjectURL(item.thumbnailFile)
              : false,
            mainFile: URL.createObjectURL(item.mainFile),
          };
          return;
        }
      }
      const target = isThumbnail ? 'thumbnail' : 'image';

      this.downloadFileFromWorker(userAvatarFileId, target).then(
        async (fileData) => {
          const data = {
            id: userAvatarFileId,
            mainFile: isThumbnail ? item?.mainFile : fileData.blob,
            thumbnailFile: isThumbnail ? fileData.blob : item?.thumbnailFile,
          };
          if (item) {
            this.usersAvatarBlobList[userAvatarFileId] = {
              thumbnailFile: isThumbnail
                ? fileData.filePath
                : item.thumbnailFile
                ? URL.createObjectURL(item.thumbnailFile)
                : false,
              mainFile: !isThumbnail
                ? fileData.filePath
                : item.mainFile
                ? URL.createObjectURL(item.mainFile)
                : false,
            };
            await updateItemInIndexDB('avatars', data);
          } else {
            this.usersAvatarBlobList[userAvatarFileId] = {
              thumbnailFile: isThumbnail ? fileData.filePath : false,
              mainFile: !isThumbnail ? fileData.filePath : false,
            };
            await addItemToIndexDB('avatars', data);
          }
        },
      );
    },
  },
  persist: false,
});
