import { defineStore } from 'pinia';
import { useUserStore } from '@/store/user/user.js';
import { initConnection } from '@/api/call/index.js';
import { getFile } from '@/helpers/parser.js';
import { useI18n } from 'vue-i18n';
import {
  getUserProfileByPhoneNumber,
  getUser,
  getUserIdPhoneNumber,
} from '@/api/user/index.js';
import { setScreenWakeLock } from '@/helpers/browserApis.js';
import { useCdrStore } from '@/store/callHistory/callHistory.js';

const baseUrl = import.meta.env['VITE_APP_SIP_WEBSOCKET_URL'];
let userAgent;
const connections = {};
let ringBackAudio;
let ringtoneAudio;
let holdAudio;

export const useCallStore = defineStore('CallStore', {
  state: () => ({
    statusOfConnection: {
      isOutgoing: null,
      isIncoming: null,
      isInConversation: null,
      isReallyConnected: null,
      intentionalEnd: null,
      calltype: '',
      callState: null,
      targetUserInformation: {
        name: '',
        avatar: '',
        phoneNumber: '',
      },
    },
    forceDoNotCloseModal: false,
    forceDoNotPlayRingback: false,
    activeCallId: null,
    activeReferalCallId: null,
    arrayForTriggerGetter: [1],
  }),
  getters: {
    getCallModalStatus(state) {
      if (
        (state.activeCallId || state.activeReferalCallId) &&
        state.arrayForTriggerGetter.length
      ) {
        const lastActiveCallId =
          state.activeReferalCallId || state.activeCallId;
        const callObject = connections[lastActiveCallId];
        const userInformation = callObject?.userInformation || {};
        if (!userInformation.name || userInformation.forceDisplayName) {
          const { t } = useI18n();
          userInformation.name =
            userInformation.forceDisplayName || t('tabs.callpad.unknown');
        }
        if (callObject) {
          return {
            isOutgoing: callObject.isOutgoing,
            isIncoming: callObject.isIncoming,
            showModal: callObject.isOutgoing || callObject.isIncoming,
            showKeys: callObject.isOutgoing || callObject.isInConversation,
            calltype: callObject.callType,
            callState: callObject.callState,
            isInConversation: callObject.isInConversation,
            isReallyConnected: callObject.isReallyConnected,
            callId: callObject.callId,
            targetUserInformation: userInformation,
            isHold:
              callObject?.session?.isOnHold().local ||
              callObject?.session?.isOnHold().local,
            isMute: callObject?.session?.isMuted().audio,
            duration: callObject.duration,
          };
        }
        return { targetUserInformation: {} };
      }
      return { targetUserInformation: {} };
    },
  },
  actions: {
    register() {
      const userStore = useUserStore();
      const displayName = userStore.nickname || userStore.name;
      const userId = userStore.userId;
      const token = userStore.token;
      userAgent = initConnection(displayName, userId, token);
      /// set connection and registred automatically
      userAgent.start();
      ////// events for userAgent
      //// registration events
      userAgent.on('registered', ({ response }) => {});
      userAgent.on('unregistered', ({ response, cause }) => {});
      userAgent.on('registrationFailed', ({ response, cause }) => {});

      ////  connection events
      /// Fired for each transport connection attempt.
      userAgent.on('connecting', ({ socket, attempts }) => {});
      /// Fired when the transport connection is established.
      userAgent.on('connected', ({ socket }) => {});
      /// Fired when the transport connection attempt (or automatic re-attempt) fails.
      userAgent.on('disconnected', ({ socket, error, code, reason }) => {});

      ///// New incoming or outgoing call event
      //Fired for an incoming or outgoing session/call.
      userAgent.on('newRTCSession', ({ originator, session, request }) => {
        // Incoming sessions
        if (session.direction == 'incoming') {
          this.incommingCallHandler(session, request).then((callId) => {
            const targetSession = connections[callId].session;
            targetSession.on('peerconnection', this.peerconnectionCallHandler);
            targetSession.on('sending', this.sendingCallHandler);
            targetSession.on('connecting', this.connectingCallHandler);
            targetSession.on('connected', this.connectedCallHandler);
            targetSession.on('accepted', this.acceptedCallHandler);
            targetSession.on('progress', this.progressEventHandler);
            targetSession.on('reinvite', this.reinviteCallHandler);
            targetSession.on('update', this.updateCallHandler);
            targetSession.on('replaces', this.replacesCallHandler);
            targetSession.on('icecandidate', this.icecandidateCallHandler);
            targetSession.on('newInfo', this.newInfoCallHandler);
            targetSession.on('info', this.infoCallHandler);
            targetSession.on('started', this.startedCallHandler);
            targetSession.on('ended', this.endedEventHandler);
            targetSession.on('refer', this.referingCallHandler);
            targetSession.on('hold', this.holdEventHandler);
            targetSession.on('unHold', this.unholdEventHandler);
            targetSession.on('sdp', this.sdpEventHandler);
            targetSession.on('confirmed', this.confirmedEventHandler);
            targetSession.on('failed', this.failedEventHandler);
            targetSession.on('referSubscriber', (data) => {
              // Update UI or perform actions on successful call transfer to Session C
            });
          });
        }
        // Outgoing sessions
        if (session.direction == 'outgoing') {
          this.outgoingCallHandler(session, request).then((callId) => {
            const targetSession = connections[callId].session;
            targetSession.on('peerconnection', this.peerconnectionCallHandler);
            targetSession.on('sending', this.sendingCallHandler);
            targetSession.on('connecting', this.connectingCallHandler);
            targetSession.on('connected', this.connectedCallHandler);
            targetSession.on('accepted', this.acceptedCallHandler);
            targetSession.on('progress', this.progressEventHandler);
            targetSession.on('reinvite', this.reinviteCallHandler);
            targetSession.on('update', this.updateCallHandler);
            targetSession.on('replaces', this.replacesCallHandler);
            targetSession.on('icecandidate', this.icecandidateCallHandler);
            targetSession.on('newInfo', this.newInfoCallHandler);
            targetSession.on('info', this.infoCallHandler);
            targetSession.on('started', this.startedCallHandler);
            targetSession.on('ended', this.endedEventHandler);
            targetSession.on('refer', this.referingCallHandler);
            targetSession.on('hold', this.holdEventHandler);
            targetSession.on('unHold', this.unholdEventHandler);
            targetSession.on('sdp', this.sdpEventHandler);
            targetSession.on('confirmed', this.confirmedEventHandler);
            targetSession.on('failed', this.failedEventHandler);
            targetSession.on('referSubscriber', (data) => {
              // Update UI or perform actions on successful call transfer to Session C
            });
          });
        }
      });
    },
    // InComing Outgoing referal call back Handler
    incommingCallHandler(session, request) {
      try {
        const targetUser = session._remote_identity; // userid || phoneNumber
        const callId = request.call_id;
        const sessionRequestHeaders = session._request.headers;
        const userStore = useUserStore();
        const selfPhoneNumber = userStore.phoneNumber;
        this.getTargetUserProfile(targetUser, callId);
        const replaces = request.headers['Replaces'];
        if (replaces) {
          connections[callId] = {
            session: session,
            userInformation: {
              name: targetUser._displayName || targetUser._uri._user,
              uri: targetUser._uri._user,
            },
            selfPhoneNumber: selfPhoneNumber,
            isInConversation: true,
            callState: 'enterpriseDid',
            isIncoming: true,
            callId: callId,
          };
          const xCallType = sessionRequestHeaders['X-Calltype'];
          // assign type of a call to it`s object
          connections[callId].callType =
            xCallType && xCallType[0] && xCallType[0].raw === 'enterprise_did'
              ? 'enterpriseDid'
              : 'freeCall';

          this.answerTheReferalCall(callId);
          return Promise.resolve(callId);
        }

        // assign a new session to connections object by CallId Key
        connections[callId] = {
          session: session,
          userInformation: {
            name: targetUser._displayName || targetUser._uri._user,
            uri: targetUser._uri._user,
          },
          selfPhoneNumber: selfPhoneNumber,

          callState: 'enterpriseDid',
          isIncoming: true,
          callId: callId,
        };

        // getting user Profile by userId or PhoneNumber
        // getting call request headers information
        // X-Calltype is array of object in header of request
        // we detect call type is free_call or etc
        const xCallType = sessionRequestHeaders['X-Calltype'];
        // assign type of a call to it`s object
        connections[callId].callType =
          xCallType && xCallType[0] && xCallType[0].raw === 'enterprise_did'
            ? 'enterpriseDid'
            : 'freeCall';
        this.activeCallId = callId;
        triggerRingtoneAudio(true);

        return Promise.resolve(callId);
      } catch (error) {}
    },
    outgoingCallHandler(session, request) {
      const targetUser = session._remote_identity; //  ._user =>userid || phoneNumber
      const callId = request.call_id;
      const userStore = useUserStore();
      const selfPhoneNumber = userStore.phoneNumber;
      this.getTargetUserProfile(targetUser, callId);
      // assign a new session to connections object by CallId Key
      connections[callId] = {
        session: session,
        userInformation: {
          name: targetUser._displayName || targetUser._uri._user,
          uri: targetUser._uri._user,
        },
        isOutgoing: true,
        callState: 'ringing',
        callId: callId,
        selfPhoneNumber,
      };

      // getting call request headers information
      const sessionRequestExtraHeaders = session._request.extraHeaders;
      // X-Calltype is array of String in header of request
      // we detect call type is free_call or nava_out and etc

      connections[callId].callType = sessionRequestExtraHeaders.includes(
        'X-callType : nava_out',
      )
        ? 'navaOut'
        : 'freeCall';

      // Sometimes we want to transfer a user to another user
      // here we detect this outgoing session is for referal
      // referal-asking in here means user (A || B) sent data of user C and ( A || B ) sent a call to User C
      // A-B-C or B-A-C
      return Promise.resolve(callId);
    },
    referingCallHandler({ request }) {
      const referedBy = request.headers['Referred-By'][0].raw;
      const replaceHeader = decodeURIComponent(
        request?.refer_to?._uri?._headers?.Replaces[0],
      );

      const callType = request.headers['X-Calltype'][0].raw;
      const option = {
        mediaConstraints: { audio: true, video: false },
        extraHeaders: [
          `X-callType : ${callType}`,
          `X-Referred-By: ${referedBy}`,
          `Referred-By: ${referedBy}`,
          `replaces: ${replaceHeader}`,
          `x-replaces: ${replaceHeader}`,
        ],
        sessionTimersExpires: 1800,
      };
      // Log or use the replacesValue as needed

      this.makeReferalCall(request.refer_to._uri._user, option);
    },

    /// making Call Actions

    // ui action Handler
    //this function detect phoneNumber that user enter in callpad tab is free_call Or Nava
    async makingCallHandler(phoneNumber) {
      const userStore = useUserStore();
      if (userStore.phoneNumber.includes(phoneNumber)) {
        return;
      }
      setScreenWakeLock(); // prevent scrren to be off after a while
      let callNumber =
        phoneNumber[0] == '0'
          ? '98' + phoneNumber.substring(1, phoneNumber.length)
          : phoneNumber;

      if (phoneNumber.endsWith('***')) {
        callNumber = callNumber.substring(0, callNumber.length - 3);
        triggerRingBackAudio(true);
        return this.makeFreeCall(callNumber);
      }
      triggerRingBackAudio(true);
      return this.makeNavaCall(callNumber);
    },
    // make free call
    async makeFreeCall(phoneNumber, isRefered) {
      const userStore = useUserStore();
      const token = userStore.token;
      const options = {
        mediaConstraints: { audio: true, video: false },
        extraHeaders: ['X-callType : free_call', `X-Token: ${token}`],
        sessionTimersExpires: 1800,
      };
      const target = `sip:${phoneNumber}@${baseUrl}`;
      isRefered ? options.extraHeaders.push(['X-Refered: ' + token]) : '';
      const callId = await userAgent.call(target, options)._request.call_id;
      this.addStream(callId);
      this.activeCallId = callId;
      return callId;
    },
    // make a nava call it`s means the Sim Cart is target
    async makeNavaCall(phoneNumber) {
      const userStore = useUserStore();
      const token = userStore.token;
      const options = {
        mediaConstraints: { audio: true, video: false },
        extraHeaders: ['X-callType : nava_out', `X-Token: ${token}`],
        sessionTimersExpires: 1800,
      };
      const target = `sip:${phoneNumber}@${baseUrl}`;
      const callId = await userAgent.call(target, options)._request.call_id;
      this.addStream(callId);
      this.activeCallId = callId;
      return callId;
    },
    // here is step one of transfer
    // this function invoke in ui component by selecting a contacts during a call or enter a number
    async makingCallByAnotherUserForAskingTransfer(phoneNumber, type) {
      const userStore = useUserStore();
      const token = userStore.token;
      if (userStore.phoneNumber.includes(phoneNumber)) {
        return;
      }
      let callType = type;
      const options = {
        mediaConstraints: { audio: true, video: false },
        extraHeaders: [`X-callType : ${callType}`, `X-Token: ${token}`],
        sessionTimersExpires: 1800,
      };
      const target = `sip:${phoneNumber}@${baseUrl}`;
      const callId = await userAgent.call(target, options)._request.call_id;
      this.activeReferalCallId = callId;
      connections[callId].transformCallType = callType;
      this.addStream(callId);
      return callId;
    },
    async makeReferalCall(phoneNumber, options) {
      const userStore = useUserStore();
      const token = userStore.token;
      const target = `sip:${phoneNumber}@${baseUrl}`;
      options.extraHeaders.push(`X-Token: ${token}`);
      const callId = await userAgent.call(target, options)._request.call_id;
      this.endingCallHandler(this.activeCallId);
      this.activeCallId = callId;
      this.addStream(callId);
      this.arrayForTriggerGetter.push({});
      return callId;
    },
    // answer actions
    answerTheCall(sessionId) {
      const userStore = useUserStore();
      const token = userStore.token;
      const connection = connections[sessionId];
      const options = {
        mediaConstraints: { audio: true, video: false },
        extraHeaders: [`X-Token: ${token}`],
        sessionTimersExpires: 1800,
      };
      connection.session.answer(options);
      connection.isInConversation = true;
      this.arrayForTriggerGetter.push({});
      triggerRingtoneAudio(false);
      this.addStream(sessionId);
    },
    async answerTheReferalCall(sessionId) {
      const userStore = useUserStore();
      connections[sessionId].isInConversation = true;
      const token = userStore.token;
      const session = connections[sessionId].session;
      const options = {
        mediaConstraints: { audio: true, video: false },
        extraHeaders: [`X-Token: ${token}`],
        sessionTimersExpires: 1800,
      };
      session.answer(options);
      triggerRingtoneAudio(false);
      this.arrayForTriggerGetter.push({});
      this.activeCallId = sessionId;
      this.addStream(sessionId);
    },
    referTheCall(phoneNumber, type) {
      getUserIdPhoneNumber(phoneNumber).then((userId) => {
        const userStore = useUserStore();
        const token = userStore.token;
        const connectionB = connections[this.activeCallId];
        const connectionC = connections[this.activeReferalCallId];
        const target = `sip:${phoneNumber}@${baseUrl}`;
        const targetReferedHeader = `<sip:${userId}@${baseUrl}>`;

        const extraHeaders = [
          `X-callType : ${type}`,
          `X-Token: ${token}`,
          `Referred-By:${targetReferedHeader} `,
          `X-Referred-By:${targetReferedHeader}`,
          `X-cid: ${userId}`,
        ];
        connectionB.session.refer(target, {
          mediaConstraints: { audio: true, video: false },
          extraHeaders: extraHeaders,
          replaces: connectionC.session,
          sessionTimersExpires: 1800,
        });
        setTimeout(() => {
          removeAudioTagFromDOM(connectionB.callId);
          removeAudioTagFromDOM(connectionC.callId);
          connectionB.isOutgoing = null;
          connectionB.isIncoming = null;
          connectionC.isOutgoing = null;
          connectionC.isIncoming = null;
        }, 0);
      });
    },

    /// End the call Actions
    // here detect call is where of a call by status
    endingCallHandler(sessionId) {
      const connection = connections[sessionId];
      if (!connection) {
        this.activeCallId = null;
        this.activeReferalCallId = null;
        setScreenWakeLock(false); // if set false its mean its dosen matterscrren be off
        return;
      }
      this.stopDuration(sessionId);
      if ([2, 9].includes(connection.session?.status)) {
        this.endTheCall(sessionId);
        connection.isOutgoing = null;
        connection.isIncoming = null;
        this.arrayForTriggerGetter.push({});
        if (
          !this.activeReferalCallId ||
          (!this.activeCallId && sessionId == this.activeReferalCallId)
        ) {
          setScreenWakeLock(false); // if set false its mean its dosen matterscrren be off
        }
        return;
      }
      if ([4, 6].includes(connection.session?.status)) {
        connection.isOutgoing = null;
        connection.isIncoming = null;
        this.arrayForTriggerGetter.push({});
        this.rejectTheCall(sessionId);
        if (
          !this.activeReferalCallId ||
          (!this.activeCallId && sessionId == this.activeReferalCallId)
        ) {
          setScreenWakeLock(false); // if set false its mean its dosen matterscrren be off
        }
      }
      connection.isOutgoing = null;
      connection.isIncoming = null;
      this.arrayForTriggerGetter.push({});
      removeAudioTagFromDOM(sessionId);
      triggerRingBackAudio(false);
      triggerRingtoneAudio(false);
    },
    endTheCall(sessionId) {
      const connection = connections[sessionId];
      if (!connection) {
        if (this.activeReferalCallId) {
          this.activeReferalCallId = null;
        }
        if (this.activeCallId) {
          this.activeCallId = null;
        }
      }
      const cdrStore = useCdrStore();
      cdrStore.addToCallHistory(connection, 'from 1');
      connection.session.terminate();
      removeAudioTagFromDOM(sessionId);
      if (this.activeReferalCallId == sessionId) {
        this.activeReferalCallId = null;
        return;
      }
      if (this.activeCallId == sessionId) {
        this.activeCallId = null;
      }
    },
    rejectTheCall(sessionId) {
      const connection = connections[sessionId];
      const optionReject = {
        status_code: 603,
        reason_phrase: 'Decline',
      };
      const cdrStore = useCdrStore();
      cdrStore.addToCallHistory(connection, 'from 2');
      connection.session.terminate(optionReject);
      removeAudioTagFromDOM(sessionId);
      triggerRingtoneAudio(false);
      triggerRingBackAudio(false);
      if (this.activeReferalCallId == sessionId) {
        this.activeReferalCallId = null;
        return;
      }
      if (this.activeCallId == sessionId) {
        this.activeCallId = null;
      }
    },
    sendDTMF(sessionId, number) {
      const connection = connections[sessionId];
      const options = {
        transportType: 'RFC2833',
      };
      if (connection.dtmfHistory) {
        const tone = number - connection.dtmfHistory * 10;
        connection.dtmfHistory = number;
        connection.session.sendDTMF(tone, options);
      } else {
        connection.session.sendDTMF(number, options);
        connection.dtmfHistory = number;
      }
    },
    startDuration(sessionId) {
      const connection = connections[sessionId];
      connection.duration = 0;
      connection.interval = setInterval(() => {
        console.count(connection.duration);
        connection.duration += 1000;
        this.arrayForTriggerGetter.push({});
        if (this.arrayForTriggerGetter.length > 30) {
          this.arrayForTriggerGetter = [{}];
        }
      }, 1000);
    },
    stopDuration(sessionId) {
      const connection = connections[sessionId];
      clearInterval(connection.interval);
      this.arrayForTriggerGetter.push({});
      this.arrayForTriggerGetter = [{}];
    },
    getConnectionObject(sessionId) {
      return connections[sessionId];
    },
    async addStream(sessionId) {
      const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
      stream.getTracks().forEach((track) => {
        track.stop();
      });
      const connection = connections[sessionId];
      if (!connection) {
        console.error('connection Not exist by this id : ', sessionId);
        return;
      }
      connection.session.connection.addEventListener('track', function (e) {
        createAudioTagAndSetStream(sessionId, e.streams[0]);
      });
    },

    speakerOn(activeSessionId) {
      triggerSpeackerHandler(activeSessionId, true);
    },
    speakerOff(activeSessionId) {
      triggerSpeackerHandler(activeSessionId, false);
    },

    holdTheCall(activeSessionId) {
      const connection = connections[activeSessionId];
      if (connection && connection.session) {
        connection.session.hold();
        connection.callState = 'hold';
        this.arrayForTriggerGetter.push({});
      }
    },
    unholdTheCall(activeSessionId) {
      const connection = connections[activeSessionId];
      if (connection && connection.session) {
        connection.session.unhold();
        connection.callState = 'inConversation';

        this.arrayForTriggerGetter.push({});
      }
    },
    muteTheCall(activeSessionId) {
      const connection = connections[activeSessionId];
      if (connection && connection.session) {
        connection.session.mute();
        this.arrayForTriggerGetter.push({});
      }
    },
    unmuteTheCall(activeSessionId) {
      const connection = connections[activeSessionId];
      if (connection && connection.session) {
        connection.session.unmute();
        this.arrayForTriggerGetter.push({});
      }
    },

    setAudioTag(ringBackAudioElement, ringtoneAudioElement, holdAudioElement) {
      ringBackAudio = ringBackAudioElement;
      ringtoneAudio = ringtoneAudioElement;
      holdAudio = holdAudioElement;
    },
    getTargetUserProfile(targetUser, callId) {
      const searchValue = targetUser._uri?._user
        ? targetUser._uri._user
        : targetUser;
      if (/^[a-z,0-9,-]{36,36}$/.test(searchValue)) {
        getUser(searchValue).then((result) => {
          this.setTargetUserProfile(result, targetUser, callId);
        });
      } else {
        getUserProfileByPhoneNumber(searchValue).then((result) => {
          this.setTargetUserProfile(result, targetUser, callId);
        });
      }
    },
    async setTargetUserProfile(information, target, callId) {
      let renderingInfo = {
        name: information.name || information.nickname || target._displayName,
        phoneNumber: information.phone_number,
        avatar: null,
      };
      if (information.avatar) {
        const userStore = useUserStore();
        const token = userStore.token;
        const fileId = information.avatar.split('.');
        const fileIdResult = fileId[0] ? fileId[0] : information.avatar;
        const filePath = await getFile(fileIdResult, token, 'image');
        renderingInfo.avatar = filePath;
        connections[callId].userInformation = renderingInfo;
        this.arrayForTriggerGetter.push({});
        return;
      }
      connections[callId].userInformation = renderingInfo;
      this.arrayForTriggerGetter.push({});
    },
    //////////////////////
    peerconnectionCallHandler(event) {},
    sendingCallHandler(event) {},
    connectingCallHandler(event) {},
    connectedCallHandler(event) {},
    acceptedCallHandler(event) {},
    reinviteCallHandler(event) {
      const callId = event.request.call_id;
      if (this.activeReferalCallId == callId) {
        const connection = connections[callId];
        const callIsHold = connections[callId]?.session?.isOnHold();
        if (callIsHold.remote) {
          connection.callState = 'inConversation';
          triggerHoldAudio(false);
        } else {
          connection.callState = 'hold';
          triggerHoldAudio(true);
        }
        return;
      } else if (this.activeCallId == callId) {
        const connection = connections[callId];
        const callIsHold = connections[callId]?.session?.isOnHold();
        if (callIsHold.remote) {
          connection.callState = 'inConversation';
          triggerHoldAudio(false);
          this.arrayForTriggerGetter.push({});
        } else {
          connection.callState = 'hold';
          triggerHoldAudio(true);
          this.arrayForTriggerGetter.push({});
        }
      }
    },
    updateCallHandler(event) {},
    replacesCallHandler(event) {
      event.accept(this.onNewSession);
    },
    onNewSession(newSession) {},
    icecandidateCallHandler(event) {
      if (event.candidate) {
        event.ready(event.candidate);
      }
    },
    newInfoCallHandler(event) {},
    infoCallHandler(event) {},
    startedCallHandler(event) {},
    progressEventHandler(event) {},
    sdpEventHandler(event) {},
    confirmedEventHandler(event) {
      try {
        const connection =
          connections[this.activeCallId || this.activeReferalCallId];
        connection.isInConversation = true;
        connection.callState = 'inConversation';
        connection.isReallyConnected = true;
        this.arrayForTriggerGetter.push({});
        triggerRingBackAudio(false);
        triggerRingtoneAudio(false);
      } catch (e) {}
    },
    holdEventHandler(event) {},
    unholdEventHandler(event) {},
    endedEventHandler(event) {
      if (event.originator == 'remote' && event.cause == 'Terminated') {
        const callId = event.message.call_id;
        this.stopDuration(callId);
        const cdrStore = useCdrStore();
        cdrStore.addToCallHistory(this.getConnectionObject(callId), 'from 3');
        if (this.activeReferalCallId == callId) {
          removeAudioTagFromDOM(callId);
          connections[callId].isOutgoing = null;
          connections[callId].isIncoming = null;
          this.arrayForTriggerGetter.push({});
          delete connections[callId];
          this.activeReferalCallId = null;
          if (
            !this.activeReferalCallId ||
            (!this.activeCallId && callId == this.activeReferalCallId)
          ) {
            setScreenWakeLock(false); // if set false its mean its dosen matterscrren be off
          }
          return;
        } else if (this.activeCallId == callId) {
          removeAudioTagFromDOM(callId);
          connections[callId].isOutgoing = null;
          connections[callId].isIncoming = null;
          this.arrayForTriggerGetter.push({});
          delete connections[callId];
          this.activeCallId = null;
          if (
            !this.activeReferalCallId ||
            (!this.activeCallId && callId == this.activeReferalCallId)
          ) {
            setScreenWakeLock(false); // if set false its mean its dosen matterscrren be off
          }
        }
      }
      triggerHoldAudio(false);
    },
    failedEventHandler(event) {
      if (
        event.originator == 'remote' &&
        [
          'Canceled',
          'Rejected',
          'Unavailable',
          'Authentication Error',
          'SIP Failure Code',
          'Not Found',
          'Forbidden',
        ].includes(event.cause)
      ) {
        const callId = event.message.call_id;
        const cdrStore = useCdrStore();
        cdrStore.addToCallHistory(this.getConnectionObject(callId), 'from 4');
        this.stopDuration(callId);
        if (this.activeReferalCallId == callId) {
          removeAudioTagFromDOM(callId);
          connections[callId].isOutgoing = null;
          connections[callId].isIncoming = null;
          this.arrayForTriggerGetter.push({});
          delete connections[callId];
          this.activeReferalCallId = null;
          triggerRingtoneAudio(false);
          triggerRingBackAudio(false);
          if (
            !this.activeReferalCallId ||
            (!this.activeCallId && sessionId == this.activeReferalCallId)
          ) {
            setScreenWakeLock(false); // if set false its mean its dosen matterscrren be off
          }
          return;
        } else if (this.activeCallId == callId) {
          connections[callId].isOutgoing = null;
          connections[callId].isIncoming = null;
          this.arrayForTriggerGetter.push({});
          delete connections[callId];
          triggerRingtoneAudio(false);
          triggerRingBackAudio(false);
          this.activeCallId = null;
          if (
            !this.activeReferalCallId ||
            (!this.activeCallId && sessionId == this.activeReferalCallId)
          ) {
            setScreenWakeLock(false); // if set false its mean its dosen matterscrren be off
          }
        }
        removeAudioTagFromDOM(callId);
      }
    },
  },
  persist: false,
});

function triggerRingtoneAudio(setPlay = true) {
  ringtoneAudio.currentTime = 0;
  if (setPlay) {
    if (ringtoneAudio.paused) {
      ringtoneAudio.play();
    }
    return;
  }
  if (!ringtoneAudio.paused) {
    ringtoneAudio.pause();
  }
}

function triggerRingBackAudio(setPlay = true) {
  ringBackAudio.currentTime = 0;
  if (setPlay) {
    if (ringBackAudio.paused) {
      ringBackAudio.play();
    }
    return;
  }
  if (!ringBackAudio.paused) {
    ringBackAudio.pause();
  }
}
function triggerHoldAudio(setPlay = true) {
  holdAudio.currentTime = 0;
  if (setPlay) {
    if (holdAudio.paused) {
      holdAudio.play();
    }
    return;
  }
  if (!holdAudio.paused) {
    holdAudio.pause();
  }
}

function createAudioTagAndSetStream(callId, stream) {
  const audioTagElement = document.createElement('audio');
  audioTagElement.setAttribute('webkit-playsinline', '');
  audioTagElement.setAttribute('playsinline', '');
  audioTagElement.setAttribute('loop', '');
  audioTagElement.setAttribute('preload', 'auto');
  audioTagElement.setAttribute('id', 'voice-audio-element' + callId);
  audioTagElement.volume = 0.1;
  audioTagElement.srcObject = stream;
  document.body.appendChild(audioTagElement);
  setTimeout(() => {
    audioTagElement.play();
  }, 0);
}
function removeAudioTagFromDOM(callId) {
  founderAudioTagElement(callId)
    .then((audioTagElement) => {
      audioTagElement.remove();
    })
    .catch((e) => {});
}

function triggerSpeackerHandler(callId, isOn) {
  founderAudioTagElement(callId)
    .then((audioTagElement) => {
      if (isOn) {
        audioTagElement.volume = 1;
        return;
      }
      audioTagElement.volume = 0.1;
    })
    .catch((e) => {});
}
// id is equal to callID
function founderAudioTagElement(id) {
  return new Promise((resolve, reject) => {
    let audioTagElement = document.getElementById('voice-audio-element' + id);
    if (audioTagElement) {
      resolve(audioTagElement);
    } else {
      let tryCounter = 0;
      const interval = setInterval(() => {
        if (audioTagElement || tryCounter > 4) {
          if (audioTagElement) {
            resolve(audioTagElement);
          } else
            reject(
              `Audio Tag Element by this id (voice-audio-element${id}) Not exist; ${tryCounter} try to access to it`,
            );
          clearInterval(interval);
        }
        audioTagElement = document.getElementById('voice-audio-element' + id);
        tryCounter++;
      }, 500);
    }
  });
}
